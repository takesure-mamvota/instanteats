using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Core.Entities;
using Core.Entities.OrderAggregate;
using Core.Interface;
using Core.Specification;

namespace Infrastructure.Service
{
     public class OrderService : IOrderService
     {
          private readonly IBasketRepository _basketRepo;
          private readonly IUnitOfWork _unitOfWork;
          public OrderService(IBasketRepository basketRepo, IUnitOfWork unitOfWork)
          {
               _unitOfWork = unitOfWork;
               _basketRepo = basketRepo;
              
          }

          public async Task<Order> CreateOrderAsync(string buyerEmail, int deliveryMethodId, string basketId, Address shippingAddress)
          {
               // get basket from the repo
               var basket = await _basketRepo.GetBasketAsync(basketId);

               // get items from the food response
               var items = new List<OrderItem>();
               foreach (var item in basket.Items)
               {
                    var foodItem = await _unitOfWork.Repository<FoodItem>().GetByIdAsync(item.Id);
                    var itemOrdered = new FoodItemOrdered(foodItem.Id, foodItem.Name,
                    foodItem.PictureUrl);
                    var orderItem = new OrderItem(itemOrdered, foodItem.Price, item.Quantity);
                    items.Add(orderItem);
               }

               // get delivery method from response
               var deliveryMethod = await _unitOfWork.Repository<DeliveryMethod>().GetByIdAsync(deliveryMethodId);

               // calc subtotal
               var subtotal = items.Sum(item => item.Price * item.Quantity);


               // create order

               var order = new Order(items, buyerEmail, shippingAddress, deliveryMethod, subtotal);
               _unitOfWork.Repository<Order>().Add(order);

               //save to db
               var result = await _unitOfWork.Complete();

               if (result <= 0) return null;

               //Delete Basket 
               await _basketRepo.DeleteBasketAsync(basketId);

               // return order
               return order;

          }

          public async Task<IReadOnlyList<DeliveryMethod>> GetDeliveryMethodAsync()
          {
               return await _unitOfWork.Repository<DeliveryMethod>().ListAllAsync();
          }

          public async Task<Order> GetOrderByIdAsync(int id, string buyerEmail)
          {
               var spec = new OrderWithItemsAndOrderingSpecification(id, buyerEmail);

               return await _unitOfWork.Repository<Order>().GetEntityWithSpec(spec);
          }

          public async Task<IReadOnlyList<Order>> GetOrdersForUserAsync(string buyerEmail)
          {
               var spec = new OrderWithItemsAndOrderingSpecification(buyerEmail);

               return await _unitOfWork.Repository<Order>().ListAsync(spec);
          }
     }
}