using Core.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Infrastructure.Config
{
     public class FoodConfiguration : IEntityTypeConfiguration<FoodItem>
     {
          public void Configure(EntityTypeBuilder<FoodItem> builder)
          {
               builder.Property(p => p.Id).IsRequired();
               builder.Property(p => p.Name).IsRequired().HasMaxLength(255);
               builder.Property(p => p.Description).IsRequired().HasMaxLength(255);
               builder.Property(p => p.Ingredients).IsRequired().HasMaxLength(255);
               builder.Property(p => p.PictureUrl).IsRequired();
               builder.Property(p => p.Price).HasColumnType("decimal(18,2)");
               builder.Property(p => p.Active).IsRequired();
               builder.HasOne(b => b.Category).WithMany()
                    .HasForeignKey(p => p.CategoryId);
               builder.HasOne(b => b.FoodType).WithMany()
                    .HasForeignKey(p => p.FoodTypeId);
               builder.HasOne(b => b.Restaurant).WithMany()
                    .HasForeignKey(p => p.RestaurantId);
          }


     }
}