using System.Collections.Generic;
using System.Threading.Tasks;
using Core.Entities;
using Core.Interface;
using Microsoft.EntityFrameworkCore;

namespace Infrastructure.Data
{
     public class FoodRepository : IFoodRepository
     {
          private readonly StoreContext _context;
          public FoodRepository(StoreContext context)
          {
               _context = context;
          }

          public async Task<IReadOnlyList<Category>> GetCategoryAsync()
          {
               return await _context.Categories.ToListAsync();
          }

          public async Task<IReadOnlyList<Restaurant>> GetRestaurantsAsync()
          {
               return await _context.Restaurants.ToListAsync();
          }

          public async Task<FoodItem> GetFoodByIdAsync(int id)
          {
               return await _context.FoodItems
                    .Include(f => f.Category)
                    .Include(f => f.FoodType)
                    .Include(f => f.Restaurant)
                    .FirstOrDefaultAsync(f => f.Id == id);


          }

          public async Task<IReadOnlyList<FoodItem>> GetFoodsAsync()
          {
               return await _context.FoodItems
                    .Include(f => f.Category)
                    .Include(f => f.FoodType)
                    .Include(f => f.Restaurant)
                    .ToListAsync();
          }

          public async Task<IReadOnlyList<FoodType>> GetFoodTypeAsync()
          {
               return await _context.FoodTypes.ToListAsync();
          }

          
     }

}