using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text.Json;
using System.Threading.Tasks;
using Core.Entities;
using Core.Entities.OrderAggregate;
using Microsoft.Extensions.Logging;

namespace Infrastructure.Data
{
    public class StoreContextSeed
    {
        public static async Task SeedAsync(StoreContext context, ILoggerFactory loggerFactory)
        {
            try
            {
                if (!context.Categories.Any())
                {
                    var categoriesData = 
                            File.ReadAllText("../Infrastructure/Data/SeedData/category.json");
                    
                    var categories = JsonSerializer.Deserialize<List<Category>>(categoriesData);

                    foreach(var category in categories)
                    {
                        context.Categories.Add(category);
                    }

                    await context.SaveChangesAsync();

                }

                if (!context.FoodTypes.Any())
                {
                    var foodTypesData = 
                            File.ReadAllText("../Infrastructure/Data/SeedData/types.json");
                    
                    var foodTypes = JsonSerializer.Deserialize<List<FoodType>>(foodTypesData);

                    foreach(var foodType in foodTypes)
                    {
                        context.FoodTypes.Add(foodType);
                    }

                    await context.SaveChangesAsync();

                }

                if (!context.FoodItems.Any())
                {
                    var foodItemsData = 
                            File.ReadAllText("../Infrastructure/Data/SeedData/food.json");
                    
                    var foodItems = JsonSerializer.Deserialize<List<FoodItem>>(foodItemsData);

                    foreach(var foodItem in foodItems)
                    {
                        context.FoodItems.Add(foodItem);
                    }

                    await context.SaveChangesAsync();

                }

                if (!context.DeliveryMethods.Any())
                {
                    var dmData = 
                            File.ReadAllText("../Infrastructure/Data/SeedData/delivery.json");
                    
                    var methods = JsonSerializer.Deserialize<List<DeliveryMethod>>(dmData);

                    foreach(var item in methods)
                    {
                        context.DeliveryMethods.Add(item);
                    }

                    await context.SaveChangesAsync();

                }

                if (!context.Restaurants.Any())
                {
                    var restaurantData = 
                            File.ReadAllText("../Infrastructure/Data/SeedData/restaurant.json");
                    
                    var methods = JsonSerializer.Deserialize<List<Restaurant>>(restaurantData);

                    foreach(var item in methods)
                    {
                        context.Restaurants.Add(item);
                    }

                    await context.SaveChangesAsync();

                }
            }
            catch (System.Exception ex)
            {
                var logger = loggerFactory.CreateLogger<StoreContextSeed>();
                logger.LogError(ex.Message);
            }
        }

    }
}