namespace API.Dtos
{
    public class FoodToReturnDto
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public string Ingredients { get; set; }
        public string PictureUrl { get; set; }
        public decimal Price { get; set; }
        public bool Active { get; set; }
        public string Category { get; set; }
        public string FoodType { get; set; }
        public string Restaurant { get; set; }
        
    }
}