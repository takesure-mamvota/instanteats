namespace API.Dtos
{
    public class OrderItemDto
    {
        public int FoodId { get; set; }
        public string FoodName { get; set; }
        public string PictureUrl { get; set; }
        public decimal Price { get; set; }
        public int Quantity { get; set; }
    }
}