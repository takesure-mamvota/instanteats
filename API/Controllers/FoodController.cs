
using System.Collections.Generic;
using System.Threading.Tasks;
using API.Dtos;
using API.Errors;
using API.Helpers;
using AutoMapper;
using Core.Entities;
using Core.Interface;
using Core.Specification;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace API.Controllers
{


     public class FoodController : BaseApiController
     {
          private readonly IGenericRepository<FoodItem> _foodsRepo;
          private readonly IGenericRepository<FoodType> _foodTypeRepo;
          private readonly IGenericRepository<Category> _categoryRepo;
          private readonly IMapper _mapper;
          private readonly IGenericRepository<Restaurant> _restaurantRepo;

          public FoodController(IGenericRepository<FoodItem> foodsRepo,
          IGenericRepository<FoodType> foodTypeRepo, IGenericRepository<Category> categoryRepo,
          IGenericRepository<Restaurant> restaurantRepo, IMapper mapper)
          {
               _restaurantRepo = restaurantRepo;
               _mapper = mapper;
               _categoryRepo = categoryRepo;
               _foodTypeRepo = foodTypeRepo;
               _foodsRepo = foodsRepo;

          }

          [HttpGet]
          public async Task<ActionResult<Pagination<FoodToReturnDto>>> GetFoods(
               [FromQuery] FoodSpecParams foodParams)
          {
               var spec = new FoodWithTypesAndCategorySpecification(foodParams);

               var countSpec = new FoodWithFiltersForCountSpecification(foodParams);

               var totalItems = await _foodsRepo.CountAsync(countSpec);

               var foods = await _foodsRepo.ListAsync(spec);

               var data = _mapper
                    .Map<IReadOnlyList<FoodItem>, IReadOnlyList<FoodToReturnDto>>(foods);

               return Ok(new Pagination<FoodToReturnDto>(foodParams.PageIndex, foodParams.PageSize, totalItems, data));

          }


          [HttpGet("{id}")]
          [ProducesResponseType(StatusCodes.Status200OK)]
          [ProducesResponseType(typeof(ApiResponse), StatusCodes.Status404NotFound)]
          public async Task<ActionResult<FoodToReturnDto>> GetFood(int id)
          {
               var spec = new FoodWithTypesAndCategorySpecification(id);

               var food = await _foodsRepo.GetEntityWithSpec(spec);

               if (food == null) return NotFound(new ApiResponse(404));

               return _mapper.Map<FoodItem, FoodToReturnDto>(food);
          }

          [HttpGet("categories")]
          public async Task<ActionResult<IReadOnlyList<Category>>> GetCategories()
          {
               return Ok(await _categoryRepo.ListAllAsync());
          }

          [HttpGet("types")]
          public async Task<ActionResult<IReadOnlyList<FoodType>>> GetFoodTypes()
          {
               return Ok(await _foodTypeRepo.ListAllAsync());
          }

          [HttpGet("restaurants")]
          public async Task<ActionResult<IReadOnlyList<Restaurant>>> GetRestaurants()
          {
               return Ok(await _restaurantRepo.ListAllAsync());
          }
     }
}