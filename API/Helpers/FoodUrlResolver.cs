using API.Dtos;
using AutoMapper;
using Core.Entities;
using Microsoft.Extensions.Configuration;

namespace API.Helpers
{
     public class FoodUrlResolver : IValueResolver<FoodItem, FoodToReturnDto, string>
     {
          private readonly IConfiguration _config;
          public FoodUrlResolver(IConfiguration config)
          {
               _config = config;
          }

          public string Resolve(FoodItem source, FoodToReturnDto destination, string destMember,
            ResolutionContext context)
          {
              if (!string.IsNullOrEmpty(source.PictureUrl))
              {
                  return _config["ApiUrl"] + source.PictureUrl;
              }

              return null;
          }
     }
}