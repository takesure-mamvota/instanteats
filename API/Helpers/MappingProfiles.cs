using API.Dtos;
using AutoMapper;
using Core.Entities;
using Core.Entities.Identity;
using Core.Entities.OrderAggregate;

namespace API.Helpers
{
     public class MappingProfiles : Profile
     {
          public MappingProfiles()
          {
               CreateMap<FoodItem, FoodToReturnDto>()
                 .ForMember(d => d.Category, o => o.MapFrom(s => s.Category.Name))
                 .ForMember(d => d.FoodType, o => o.MapFrom(s => s.FoodType.Name))
                 .ForMember(d => d.PictureUrl, o => o.MapFrom<FoodUrlResolver>());
               CreateMap<Core.Entities.Identity.Address, AddressDto>().ReverseMap();
               CreateMap<CustomerBasketDto, CustomerBasket>();
               CreateMap<BasketItemDto, BasketItem>();
               CreateMap<AddressDto, Core.Entities.OrderAggregate.Address>();
               CreateMap<Order, OrderToReturnDto>()
                    .ForMember(d => d.DeliveryMethod, o => o.MapFrom(s => s.DeliveryMethod.ShortName))
                    .ForMember(d => d.ShippingPrice, o => o.MapFrom(s => s.DeliveryMethod.Price));
               CreateMap<OrderItem, OrderItemDto>()
                    .ForMember(d => d.FoodId, o => o.MapFrom(s => s.ItemOrdered.FoodItemId))
                    .ForMember(d => d.FoodName, o => o.MapFrom(s => s.ItemOrdered.FoodName))
                    .ForMember(d => d.PictureUrl, o => o.MapFrom(s => s.ItemOrdered.PictureUrl))
                    .ForMember(d => d.PictureUrl, o => o.MapFrom<OrderItemUrlResolver>());

          }
     }
}