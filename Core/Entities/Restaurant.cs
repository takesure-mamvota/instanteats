namespace Core.Entities
{
    public class Restaurant : BaseEntity
    {
        public string RestaurantName { get; set; }
        public string Location { get; set; }
        public string TimeOpen { get; set; }
        public string TimeClose { get; set; }
         
    }
}