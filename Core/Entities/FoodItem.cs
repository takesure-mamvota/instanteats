namespace Core.Entities
{
    public class FoodItem : BaseEntity
    {
        public string Name { get; set; }
        public string Description { get; set; }
        public string Ingredients { get; set; }
        public string PictureUrl { get; set; }
        public decimal Price { get; set; }
        public bool Active { get; set; }
        public Category Category { get; set; }
        public int CategoryId { get; set; }
        public FoodType FoodType { get; set; }
        public int FoodTypeId { get; set; }
        public Restaurant Restaurant { get; set; }
        public int RestaurantId { get; set; }
        
    }
}