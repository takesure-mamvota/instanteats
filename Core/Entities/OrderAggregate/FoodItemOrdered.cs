namespace Core.Entities.OrderAggregate
{
    public class FoodItemOrdered
    {
        public FoodItemOrdered()
        {
        }

        public FoodItemOrdered(int foodItemId, string foodName, string pictureUrl)
        {
            FoodItemId = foodItemId;
            FoodName = foodName;
            PictureUrl = pictureUrl;
        }

        public int FoodItemId { get; set; }
        public string FoodName { get; set; }
        
        public string PictureUrl { get; set; }
        
        
    }
}