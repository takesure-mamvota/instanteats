using System.Collections.Generic;
using System.Threading.Tasks;
using Core.Entities;

namespace Core.Interface
{
    public interface IFoodRepository
    {
         Task<IReadOnlyList<FoodItem>> GetFoodsAsync();
         Task<FoodItem> GetFoodByIdAsync(int id);
         Task<IReadOnlyList<Category>> GetCategoryAsync();
         Task<IReadOnlyList<FoodType>> GetFoodTypeAsync();
         Task<IReadOnlyList<Restaurant>> GetRestaurantsAsync();
    }
}