using System;
using System.Linq.Expressions;
using Core.Entities;

namespace Core.Specification
{
     public class FoodWithTypesAndCategorySpecification : BaseSpecification<FoodItem>
     {
          public FoodWithTypesAndCategorySpecification(FoodSpecParams foodParams)
              : base(x => 
                (string.IsNullOrEmpty(foodParams.Search) || x.Name.ToLower()
                  .Contains(foodParams.Search)) &&
                (!foodParams.categoryId.HasValue || x.CategoryId == foodParams.categoryId) && 
                (!foodParams.TypeId.HasValue || x.FoodTypeId == foodParams.TypeId)
              )
          {
              AddInclude(x => x.Category);
              AddInclude(x => x.FoodType);
              AddOrderBy(x => x.Name);
              ApplyPaging(foodParams.PageSize * (foodParams.PageIndex - 1), foodParams.PageSize);

              if (!string.IsNullOrEmpty(foodParams.Sort))
              {
                  switch (foodParams.Sort)
                  {
                      case "priceAsc":
                        AddOrderBy(p => p.Price);
                        break;
                      case "priceDesc":
                        AddOrderByDescending(p => p.Price);
                        break;

                      default:
                        AddOrderBy(n => n.Name);
                        break;
                  }
              }
          }

          public FoodWithTypesAndCategorySpecification(int id) : base(x => x.Id == id)
          {
              AddInclude(x => x.Category);
              AddInclude(x => x.FoodType);
          }
     }
}