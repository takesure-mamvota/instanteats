using Core.Entities;

namespace Core.Specification
{
     public class FoodWithFiltersForCountSpecification : BaseSpecification<FoodItem>
     {
          public FoodWithFiltersForCountSpecification(FoodSpecParams foodParams)
                : base(x => 
                (string.IsNullOrEmpty(foodParams.Search) || x.Name.ToLower()
                  .Contains(foodParams.Search)) &&
                (!foodParams.categoryId.HasValue || x.CategoryId == foodParams.categoryId) && 
                (!foodParams.TypeId.HasValue || x.FoodTypeId == foodParams.TypeId)
              )
          {

          }
     }
}